//模拟数据
export default {
  //首页信息
  indexInfo: {
    luckyPrize: {
      alreadyDraw: 1,
      prizeName: 'Kindle阅读器',
      prizeImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl4jc3eq/32edf4f6-0833-42b4-a7a1-d1531c9a85bd_w281_h156.png'
    },
    newPrizeStatus: -1,
    recentPrizePeople: [
      {nickname: '我是一个用户名', prizeName: '3元红包'},
      {nickname: '我是一个用户名1我是一个用户名1', prizeName: '31元红包'},
      {nickname: '我是一个用户名2', prizeName: '23元红包'},
      {nickname: '我是一个用户名3', prizeName: '5元红包'},
      {nickname: '我是一个用户名4', prizeName: '7元红包'}
    ]
  },
  //每周抽奖活动状态返回
  getWeeklyActivityStatus: {
    activityStatus: 0
  },
  //每日活动状态返回
  getActivityStatus: {
    activityStatus: 0
  },
  draw: {
    code: 1,
    prizeName: 'Kindle阅读器',
    prizeImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl4jc3eq/32edf4f6-0833-42b4-a7a1-d1531c9a85bd_w281_h156.png'
  },
  //提交游戏结果返回数据
  submitResult: {
    historyScore: 89.99,
    newScoreStatus: 1
  },
  //我的奖品数据列表
  getMyPrize: {
    phoneStatus: 1,
    myPrize: [
      {
        date: '20180810',
        deliverDate: '20180812',
        prizeName: '10元抵扣券',
        rank: 301,
        status: 0,
        type: 0
      },
      {
        date: '20180815',
        deliverDate: '20180816',
        prizeName: '星巴克储值卡',
        rank: 3,
        status: 1,
        type: 0
      },
      {
        date: '20180815',
        deliverDate: '20180816',
        prizeName: '星巴克储值卡',
        rank: 3,
        status: 1,
        type: 0
      },
      {
        date: '20180815',
        deliverDate: '20180816',
        prizeName: 'kindle',
        rank: null,
        status: -1,
        type: 1
      },
      {
        date: '20180815',
        deliverDate: '20180816',
        prizeName: '星巴克储值卡',
        rank: 3,
        status: 1,
        type: 0
      },
      {
        date: '20180815',
        deliverDate: '20180816',
        prizeName: 'kindle',
        rank: null,
        status: -1,
        type: 1
      },
      {
        date: '20180815',
        deliverDate: '20180816',
        prizeName: '星巴克储值卡',
        rank: 3,
        status: 1,
        type: 0
      },
      {
        date: '20180815',
        deliverDate: '20180816',
        prizeName: 'kindle',
        rank: null,
        status: -1,
        type: 1
      },
      {
        date: '20180815',
        deliverDate: '20180816',
        prizeName: '星巴克储值卡',
        rank: 3,
        status: 1,
        type: 0
      },
      {
        date: '20180815',
        deliverDate: '20180816',
        prizeName: 'kindle',
        rank: null,
        status: -1,
        type: 1
      }
    ]
  },
  //填写手机号
  submitPhone: {
    code: 0
  },
  //查看排行榜
  getRankingList: {
    myRank: 12012,
    myTime: 21,
    rankingList: [
      {nickName: '我是用户0', timeUsed: 12.23},
      {nickName: '我是用户1', timeUsed: 22.12},
      {nickName: '我是用户2', timeUsed: 11.2},
      {nickName: '我是用户3', timeUsed: 22.23},
      {nickName: '我是用户4', timeUsed: 16.26},
      {nickName: '我是用户5', timeUsed: 62.23},
      {nickName: '我是用户6', timeUsed: 12.20},
      {nickName: '我是用户7', timeUsed: 62.23},
      {nickName: '我是用户8', timeUsed: 12.23},
      {nickName: '我是用户9', timeUsed: 22.12},
      {nickName: '我是用户10', timeUsed: 11.2},
      {nickName: '我是用户11', timeUsed: 22.23},
      {nickName: '我是用户12', timeUsed: 16.26},
      {nickName: '我是用户13', timeUsed: 62.23},
      {nickName: '我是用户14', timeUsed: 12.20},
      {nickName: '我是用户15', timeUsed: 62.23}
    ]
  },
  //配置信息
  commonConfig: {
    //图片
    commonImg: {
      //---------------------------------------------------主页------------------------------------------------------------
      //背景
      homeBg: 'https://gw.alipayobjects.com/os/q/cms/images/jlhp914c/0ca2f94c-9ab2-4dd5-9512-773afda47111_w750_h2530.jpg',
      //标题
      homeImgTitle: 'https://dollarcdn.cdollar.cn/huabao/234dafac-567e-42d5-bed1-5b649811495dtitle.png',
      //规则
      homeImgRule: 'https://gw.alipayobjects.com/os/q/cms/images/jl3gjw5e/19f7cb18-a31f-4120-aa1e-cb0025bc931f_w701_h1362.png',
      //大礼包
      homeBtnGift: 'https://dollarcdn.cdollar.cn/huabao/76a7f5d2-68ce-4a62-83a5-39f7c02c85bdprize.png',
      //我的奖品按钮
      homeBtnMyAward: 'https://gw.alipayobjects.com/os/q/cms/images/jl3gkkz4/142ee097-0cac-454f-9401-0ab012e0b601_w198_h67.png',
      //查看排行按钮
      homeBtnRank: 'https://dollarcdn.cdollar.cn/huabao/d5dcd84b-e4d1-4133-992b-ad298b2c4f8ebtn-查看排行.png',
      //抽奖入口按钮
      homeBtnDraw: 'https://dollarcdn.cdollar.cn/huabao/811bedfc-ba2d-4364-a9eb-be85e8fe62c1btn-抽奖入口.png',
      //开始游戏按钮
      homeBtnStart: 'https://dollarcdn.cdollar.cn/huabao/5fcdc6c6-e4ab-46cb-b42e-1491d2f398ab开始游戏.png',
      //感叹号红点图标
      homeImgRedPoint: 'https://gw.alipayobjects.com/os/q/cms/images/jl9eopch/5e20c9c3-3716-436f-b0b0-83053a54ded3_w25_h25.png',

      //---------------------------------------------------弹框------------------------------------------------------------
      //按钮背景
      msgboxBtn: 'https://gw.alipayobjects.com/os/q/cms/images/jl3h6q8a/b88effb6-a9bf-48f3-9e05-e2cb961e34a4_w381_h81.png',
      //弹窗红带
      msgboxBgRedTie: 'https://gw.alipayobjects.com/os/q/cms/images/jl4j9qjs/2eb6b090-4031-49b9-9190-c622394b7d60_w386_h102.png',
      //弹窗短背景
      msgboxBgShort: 'https://gw.alipayobjects.com/os/q/cms/images/jl4j8ojv/02ec7a73-6695-460c-823d-b32acf853f1b_w566_h385.png',
      //弹窗中背景
      msgboxBgNormal: 'https://gw.alipayobjects.com/os/q/cms/images/jl4j98m8/5d8f70b7-ad58-42a5-9178-ff49646d421f_w566_h509.png',
      //弹窗长背景
      msgboxBgLong: 'https://gw.alipayobjects.com/os/q/cms/images/jl3h7zbz/bd7826da-1711-4dfb-8cfa-b220bf82df5b_w566_h829.png',
      //弹窗纸背景
      msgboxBgPaper: 'https://gw.alipayobjects.com/os/q/cms/images/jl6ls15o/d92f0a7f-8328-4c60-949a-9f8d7c937567_w604_h505.png',
      //弹窗输入框背景
      msgboxBgInput: 'https://gw.alipayobjects.com/os/q/cms/images/jl4jbs30/739abad0-6f3e-4ef4-84e4-01db63a3ca62_w420_h78.png',
      //成分股介绍文字背景
      msgboxBgIntro: 'https://gw.alipayobjects.com/os/q/cms/images/jl3h7eec/818f348d-2a4e-4209-a4d6-24c797ac046f_w453_h392.png',
      //开始游戏按钮
      msgboxBtnStart: 'https://gw.alipayobjects.com/os/q/cms/images/jl3h8eqx/4aea8037-9682-41c4-9bfd-03f6524c40ff_w357_h81.png',
      //哭脸
      msgboxImgCryStar: 'https://gw.alipayobjects.com/os/q/cms/images/jl3h8pde/0e498953-d161-4ecf-9c6b-dc015d068686_w166_h167.png',
      //滚动牌子背景
      msgboxImgMarqueeBg: 'https://gw.alipayobjects.com/os/q/cms/images/jl3h90lj/ddea48ea-7d87-4086-aecf-46d43c6dbe07_w454_h255.png',
      //三星
      msgboxImgTriStar: 'https://gw.alipayobjects.com/os/q/cms/images/jl3hatuo/52a9c044-dcf4-45fb-8c92-0743a0f2cae0_w434_h163.png',
      //close
      msgboxBtnClose: 'https://gw.alipayobjects.com/os/q/cms/images/jl3hbfsx/c530de54-ed28-4bc0-b7fb-591277e4d98a_w53_h53.png',
      //新纪录标签
      msgboxImgNew: 'https://gw.alipayobjects.com/os/q/cms/images/jl3hh0os/734d4e6d-22f2-40a4-924c-b3e4b92c5251_w65_h38.png',
      //取消按钮
      msgboxBtnCancel: 'https://gw.alipayobjects.com/os/q/cms/images/jlbmfy58/bbeb8b1c-0d9e-4d3a-bc3c-93b5757f76d1_w205_h81.png',
      //重试按钮
      msgboxBtnConfirm: 'https://gw.alipayobjects.com/os/q/cms/images/jlbmgkvw/b2b4129f-8a69-45b3-b445-3cb9dafdd394_w205_h81.png',

      //---------------------------------------------------游戏页------------------------------------------------------------
      //红带
      gameBgRedTie: 'https://gw.alipayobjects.com/os/q/cms/images/jl3iyvkk/99ad51e3-969c-4694-851d-64c50f4eb798_w398_h96.png',
      //时间背景
      gameBgTime: 'https://dollarcdn.cdollar.cn/huabao/97bab5f8-a0c9-43cf-9729-fda5f2fa6c46倒计时底框.png',
      //时钟
      gameImgClock: 'https://dollarcdn.cdollar.cn/huabao/9a69ffa4-dbb0-43b0-a80a-7a824cebbf31钟.png',
      //进度条
      gameImgProgress: 'https://dollarcdn.cdollar.cn/huabao/9cc72e74-b7d3-4e01-96d9-dee63cc36d20进度条满.png',
      //游戏盘
      gameBgInner: 'https://gw.alipayobjects.com/os/q/cms/images/jl3j2f6z/abdb228a-e695-402c-9ed2-fecf404197c0_w727_h890.png',
      //背景
      gameBg: 'https://gw.alipayobjects.com/os/q/cms/images/jlhpcmhk/a2967e2d-f3d3-4a69-b787-07204d33fb2d_w750_h1562.jpg',
      //牌背
      gameImgCardBack: 'https://gw.alipayobjects.com/os/q/cms/images/jl55q0li/244ddb8e-0bd1-47dd-9aea-769ff4d7bcd4_w176_h177.png',
      //百胜logo
      logoBaisheng: 'https://gw.alipayobjects.com/os/q/cms/images/jl540k28/6ec87570-0695-45d8-b1c5-ff92a3f817bb_w180_h179.png',
      //迪士尼logo
      logoDisney: 'https://gw.alipayobjects.com/os/q/cms/images/jl540uez/6de3d7d7-504a-4484-9979-b54f40596106_w180_h179.png',
      //蒂芙尼logo
      logoTiffany: 'https://gw.alipayobjects.com/os/q/cms/images/jlhpbr1m/905474bf-74a9-43cb-a038-ac0b79d0e064_w180_h179.png',
      //福特logo
      logoFord: 'https://gw.alipayobjects.com/os/q/cms/images/jl542k88/a72550aa-8b4a-4b03-b938-03d04ba17a6c_w180_h179.png',
      //哈雷摩托logo
      logoHarley: 'https://gw.alipayobjects.com/os/q/cms/images/jl543ymm/214f96c5-06ce-4211-b63f-90c6f55c1875_w180_h179.png',
      //麦当劳logo
      logoMcDonalds: 'https://gw.alipayobjects.com/os/q/cms/images/jl5449vh/b68b9d08-a1ea-4b7b-a9d9-436b29358e13_w180_h179.png',
      //耐克logo
      logoNike: 'https://gw.alipayobjects.com/os/q/cms/images/jl544myp/10b5a746-d119-4109-a356-35914c5b57cf_w180_h179.png',
      //维秘logo
      logoVictorias: 'https://gw.alipayobjects.com/os/q/cms/images/jl544xre/869f1bee-f1f1-4f1f-96e7-15787514f15b_w180_h179.png',
      //星巴克logo
      logoStarbucks: 'https://gw.alipayobjects.com/os/q/cms/images/jl545cl0/62480920-f257-4ec4-b1a4-7d665a4a9369_w180_h179.png',
      //亚马逊logo
      logoAmazon: 'https://gw.alipayobjects.com/os/q/cms/images/jl545nay/436cdc89-61d9-4977-ab57-96e0423f5d37_w180_h179.png',
      //ualogo
      logoUa: 'https://gw.alipayobjects.com/os/q/cms/images/jl545zwj/5c3679c8-a425-489f-be88-d06c4500c455_w180_h179.png',

      //---------------------------------------------------排行榜页------------------------------------------------------------
      //背景
      rankBg: 'https://gw.alipayobjects.com/os/q/cms/images/jl68pa4w/961695da-337d-498c-b0c1-77e65d5708ce_w750_h2088.png',
      //标题图片
      rankImgTitle: 'https://gw.alipayobjects.com/os/q/cms/images/jl68pnin/663f569f-5e96-4f78-aa26-9efbd624de1e_w550_h209.png',
      //奖品说明
      rankBgExplain: 'https://gw.alipayobjects.com/os/q/cms/images/jl68q1w0/00a3b641-7c4d-4141-99d3-31db0cac4caa_w701_h731.png',
      //自己排行背景
      rankBgSelf: 'https://gw.alipayobjects.com/os/q/cms/images/jl68qeji/4b1047d8-0cc5-4be1-8a85-06147679489e_w694_h260.png',
      //排行榜背景
      rankBgAll: 'https://gw.alipayobjects.com/os/q/cms/images/jl68rjvs/3da59d62-fd47-4343-8cb6-d74355ff9dab_w705_h868.png',
      //金牌
      rankImgGold: 'https://gw.alipayobjects.com/os/q/cms/images/jl68rzxu/bed62946-727f-4b4e-93ad-89d1d61c48b6_w40_h53.png',
      //银牌
      rankImgSilver: 'https://gw.alipayobjects.com/os/q/cms/images/jl68seue/c788b2aa-5e3e-4799-bffc-110c1a103289_w40_h53.png',
      //铜牌
      rankImgCopper: 'https://gw.alipayobjects.com/os/q/cms/images/jl68sqpk/0f91aca2-0843-4b7c-baaa-361fe9b59d71_w40_h53.png',

      //---------------------------------------------------我的奖品页------------------------------------------------------------
      //背景
      awardBg: 'https://gw.alipayobjects.com/os/q/cms/images/jl6hu2ui/f124a0bc-d15a-42ec-9a50-458db308eb99_w750_h2269.png',
      //标题图片
      awardImgTitle: 'https://gw.alipayobjects.com/os/q/cms/images/jl6hul0u/268c51ed-c547-4af8-9f2e-97e1a75f2c84_w400_h126.png',
      //奖品栏背景
      awardBgStatus: 'https://gw.alipayobjects.com/os/q/cms/images/jl6hv5e7/9f282720-ac12-4836-82e2-7ba3b31f634c_w717_h867.png',
      //按钮
      awardBtn: 'https://gw.alipayobjects.com/os/q/cms/images/jlbmh467/d199679f-7d43-438b-bc60-3e57fd5fb519_w462_h87.png',
      //游戏规则
      awardImgRule: 'https://gw.alipayobjects.com/os/q/cms/images/jl6hvtya/5e4ad611-7b1c-49f8-a613-d59b4f4f63b7_w739_h1089.png',

      //---------------------------------------------------抽奖页------------------------------------------------------------
      //背景
      drawBg: 'https://dollarcdn.cdollar.cn/huabao/690c6dd5-40be-4a42-8b36-d3d907d7cb73bg2.jpg',
      //标题图片
      drawImgTitle: 'https://dollarcdn.cdollar.cn/huabao/a2780f2e-784a-437b-9160-74e0678396de抽奖页标题.png',
      //大奖图片
      drawImgAward: 'https://dollarcdn.cdollar.cn/huabao/1984b10f-d817-46a6-a651-8b1eaf2046f0礼花.png',
      //按钮
      drawBtn: 'https://gw.alipayobjects.com/os/q/cms/images/jl7dv3ai/e018be34-4859-40d0-8612-30787a55aee1_w477_h99.png',

      //-----------------------------奖品图片(根据配置返回，字段名可随意命名，前端不会使用字段名，只做预加载)------------------------------------------------------------
      //kindle
      kindle: 'https://gw.alipayobjects.com/os/q/cms/images/jl4jc3eq/32edf4f6-0833-42b4-a7a1-d1531c9a85bd_w281_h156.png',
      //红包
      redPacket: 'http://tianpei-dong-pub.oss-cn-shenzhen.aliyuncs.com/huabao/pic_rb.png',
      //星巴克
      starbucks: 'https://gw.alipayobjects.com/os/q/cms/images/jl7drqsv/3774fd3a-055d-4b51-bbb0-b4e21b698406_w281_h170.png',

      //首页查看更多跳转Url
      urlHome2More: 'alipays://platformapi/startapp?appId=60000148&appClearTop=false&startMultApp=YES&showOptionMenu=NO&transparentTitle=auto&transparentTitleTextAuto=YES&url=%2Fwww%2Fshop.html%3Fpid%3D2088621969296491',

      //banner链接
      bannerUrl: 'alipays://platformapi/startapp?appId=60000148&appClearTop=false&startMultApp=YES&showOptionMenu=NO&transparentTitle=auto&transparentTitleTextAuto=YES&url=%2Fwww%2Fshop.html%3Fpid%3D2088621969296491',

      //banner图片
      bannerImg: 'https://gw.alipayobjects.com/os/q/cms/images/jldhvec8/306e125b-8a5d-46e7-9763-ebd2426803e0_w750_h230.png',
      //copyright图片
      copyrightImg: 'https://gw.alipayobjects.com/os/q/cms/images/jldhvz8d/50f42fa7-bc0a-4f6e-8175-0a1395090648_w749_h152.png'
    },

    //配置游戏
    levelInfo: [
      {
        level: 1,
        time: 30,
        fund: [
          {
            fundName: '百胜',
            fundIntroduce: '亚马逊，美国消费（162415）的第一重仓股！也是全球市值最高的公司之一。它作为美国网络新零售帝国，几乎在进入的所有领域都展现出了杰出的成长性。而创始人兼CEO杰夫·贝索斯，也打败比尔盖茨成为新一任世界首富！',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl540k28/6ec87570-0695-45d8-b1c5-ff92a3f817bb_w180_h179.png',
            times: 4
          },
          {
            fundName: '迪士尼',
            fundIntroduce: '迪士尼777迪士尼777迪士尼777迪士尼777迪士尼777迪士尼777',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl540uez/6de3d7d7-504a-4484-9979-b54f40596106_w180_h179.png',
            times: 2
          },
          {
            fundName: '蒂芙尼',
            fundIntroduce: '蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl5416iu/8b9d3783-6815-4eeb-b9b9-2c4785caccaf_w180_h179.png',
            times: 2
          }
        ]
      },
      {
        level: 2,
        time: 50,
        fund: [
          {
            fundName: '百胜',
            fundIntroduce: '百胜666百胜666百胜666百胜666百胜666百胜666百胜666',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl540k28/6ec87570-0695-45d8-b1c5-ff92a3f817bb_w180_h179.png',
            times: 2
          },
          {
            fundName: '迪士尼',
            fundIntroduce: '迪士尼777迪士尼777迪士尼777迪士尼777迪士尼777迪士尼777',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl540uez/6de3d7d7-504a-4484-9979-b54f40596106_w180_h179.png',
            times: 4
          },
          {
            fundName: '蒂芙尼',
            fundIntroduce: '蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl5416iu/8b9d3783-6815-4eeb-b9b9-2c4785caccaf_w180_h179.png',
            times: 2
          },
          {
            fundName: '福特',
            fundIntroduce: '福特222福特222福特222福特222福特222福特222福特222',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl542k88/a72550aa-8b4a-4b03-b938-03d04ba17a6c_w180_h179.png',
            times: 2
          },
          {
            fundName: '哈雷摩托',
            fundIntroduce: '哈雷摩托333哈雷摩托333哈雷摩托333哈雷摩托333哈雷摩托333哈雷摩托333',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl543ymm/214f96c5-06ce-4211-b63f-90c6f55c1875_w180_h179.png',
            times: 4
          },
          {
            fundName: '麦当劳',
            fundIntroduce: '麦当劳444麦当劳444麦当劳444麦当劳444麦当劳444',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl5449vh/b68b9d08-a1ea-4b7b-a9d9-436b29358e13_w180_h179.png',
            times: 2
          }
        ]
      },
      {
        level: 3,
        time: 90,
        fund: [
          {
            fundName: '百胜',
            fundIntroduce: '百胜666百胜666百胜666百胜666百胜666百胜666百胜666',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl540k28/6ec87570-0695-45d8-b1c5-ff92a3f817bb_w180_h179.png',
            times: 2
          },
          {
            fundName: '迪士尼',
            fundIntroduce: '迪士尼777迪士尼777迪士尼777迪士尼777迪士尼777迪士尼777',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl540uez/6de3d7d7-504a-4484-9979-b54f40596106_w180_h179.png',
            times: 2
          },
          {
            fundName: '蒂芙尼',
            fundIntroduce: '蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111蒂芙尼111',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl5416iu/8b9d3783-6815-4eeb-b9b9-2c4785caccaf_w180_h179.png',
            times: 2
          },
          {
            fundName: '福特',
            fundIntroduce: '福特222福特222福特222福特222福特222福特222福特222',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl542k88/a72550aa-8b4a-4b03-b938-03d04ba17a6c_w180_h179.png',
            times: 2
          },
          {
            fundName: '哈雷摩托',
            fundIntroduce: '哈雷摩托333哈雷摩托333哈雷摩托333哈雷摩托333哈雷摩托333哈雷摩托333',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl543ymm/214f96c5-06ce-4211-b63f-90c6f55c1875_w180_h179.png',
            times: 2
          },
          {
            fundName: '麦当劳',
            fundIntroduce: '麦当劳444麦当劳444麦当劳444麦当劳444麦当劳444',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl5449vh/b68b9d08-a1ea-4b7b-a9d9-436b29358e13_w180_h179.png',
            times: 2
          },
          {
            fundName: '耐克',
            fundIntroduce: '耐克555耐克555耐克555耐克555',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl544myp/10b5a746-d119-4109-a356-35914c5b57cf_w180_h179.png',
            times: 2
          },
          {
            fundName: '维秘',
            fundIntroduce: '维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666维秘666',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl544xre/869f1bee-f1f1-4f1f-96e7-15787514f15b_w180_h179.png',
            times: 2
          },
          {
            fundName: '星巴克',
            fundIntroduce: '星巴克777星巴克777星巴克777星巴克777',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl545cl0/62480920-f257-4ec4-b1a4-7d665a4a9369_w180_h179.png',
            times: 2
          },
          {
            fundName: '亚马逊',
            fundIntroduce: '亚马逊888亚马逊888亚马逊888亚马逊888亚马逊888',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl545nay/436cdc89-61d9-4977-ab57-96e0423f5d37_w180_h179.png',
            times: 2
          },
          {
            fundName: 'UA',
            fundIntroduce: 'UA999UA999UA999UA999UA999UA999UA999UA999',
            fundImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl545zwj/5c3679c8-a425-489f-be88-d06c4500c455_w180_h179.png',
            times: 4
          }
        ]
      }
    ]
  }
}
