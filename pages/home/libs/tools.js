const tools = {
  getQueryString(name) {
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null) return  unescape(r[2]);
     return null;
  },

  checkMobile (tel) {
    if(!(/^1[3|4|5|7|8]\d{9}$/.test(tel))){
      // alert("请输入正确的手机号");
      return false;
    } else {
      return true;
    }
  },
  //打乱数组
  shuffle (array) {
    var _array = array.concat()
    for (var i = _array.length; i--;) {
      var j = Math.floor(Math.random() * (i + 1))
      var temp = _array[i]
      _array[i] = _array[j]
      _array[j] = temp
    }
    return _array
  },
  /* 提取日期
   * 入参: dateNum(String), 格式可为20180825、0825、25
   * 出参: dateObj(Object), {year: '2018年', month: '8月', date: '25日'}
   */
  dateNum2dateObj (dateNum) {
    var year = null,
        month = null,
        date = null
    if (typeof(dateNum) === 'string') {
      switch (dateNum.length) {
        case 8: {
          year = dateNum.substr(0, 4) + '年'
          month = dateNum[4] === '0' ? dateNum[5] + '月' : dateNum.substr(4, 2) + '月'
          date = dateNum[6] === '0' ? dateNum[7] + '日' : dateNum.substr(6, 2) + '日'
        }break
        case 4: {
          month = dateNum[0] === '0' ? dateNum[1] + '月' : dateNum.substr(0, 2) + '月'
          date = dateNum[2] === '0' ? dateNum[3] + '日' : dateNum.substr(2, 2) + '日'
        }break
        case 2: {
          date = dateNum[0] === '0' ? dateNum[1] + '日' : dateNum + '日'
        }break
        default: ;
      }
    }
    return {
      year,
      month,
      date
    }
  },
  //获取浏览器transitionend事件
  whichTransitionEvent(){
    var t;
    var el = document.createElement('fakeelement');
    var transitions = {
        'transition':'transitionend',
        'OTransition':'oTransitionEnd',
        'MozTransition':'transitionend',
        'WebkitTransition':'webkitTransitionEnd'
    }
    for(t in transitions){
        if( el.style[t] !== undefined ){
           return transitions[t];
        }
    }
  },
  //获取浏览器animationend事件
  whichAnimationEvent(){
    var t,
        el = document.createElement("fakeelement");

    var animations = {
      "animation"      : "animationend",
      "OAnimation"     : "oAnimationEnd",
      "MozAnimation"   : "animationend",
      "WebkitAnimation": "webkitAnimationEnd"
    }

    for (t in animations){
      if (el.style[t] !== undefined){
        return animations[t];
      }
    }
  }
}

export default {
  install (Vue) {
    Vue.mixin({
      created () {
        this.$_tools = tools
      }
    })
  }
}
