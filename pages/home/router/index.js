import Vue from 'vue/dist/vue.min.js'
import Router from 'vue-router'
import Views from '../views/entry'
import App from '../app'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'loading',
      component: Views.Loading
    },
    {
      path: '/home',
      name: 'App',
      component: App,
      children: [
        {
          path: '',
          name: 'home',
          component: Views.Home
        },
        {
          path: '/game',
          name: 'game',
          component: Views.Game
        },
        {
          path: '/rank',
          name: 'rank',
          component: Views.Rank
        },
        {
          path: '/award',
          name: 'award',
          component: Views.Award
        },
        {
          path: '/draw',
          name: 'draw',
          component: Views.Draw
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  //确保每次进入路由时window对象有全局配置
  const timer = setInterval(() => {
    if (window.GLOBAL_CONFIG) {
      clearInterval(timer)
      next()
    }
  }, 20)
  setTimeout(() => {
    if (!window.GLOBAL_CONFIG) {
      clearInterval(timer)
      alert('活动太火爆，请刷新再试试吧！')
    }
  },2000)
})

export default router
