import http from './httpSettings'
import mockData from '../libs/mock'

const mockSwitch = globalSchemaData.dev.mockSwitch

const api = {
  //获取配置信息
  getCommonConfig () {
    if (mockSwitch) {
      console.log('mock-getCommonConfig')
      return new Promise ((resolve, reject) => {
        resolve(mockData.commonConfig)
      })
    } else {
      return http.get('commonConfig')
              .then(data => {
                  return Promise.resolve(data)
              })
              .catch(err => {
                  return Promise.reject(err)
              })
    }
  },
  //获取首页信息
  getIndexInfo () {
    if (mockSwitch) {
      console.log('mock-getIndexInfo')
      return new Promise ((resolve, reject) => {
        resolve(mockData.indexInfo)
      })
    } else {
      let data = {
        userId: sessionStorage.getItem('_userId'),
        nickname: sessionStorage.getItem('_nickname'),
        headImgUrl: sessionStorage.getItem('_headImgUrl')
      }
      return http.post('index', data)
              .then(data => {
                  return Promise.resolve(data)
              })
              .catch(err => {
                  return Promise.reject(err)
              })
    }
  },
  //获取每周抽奖活动状态
  getWeeklyActivityStatus () {
    if (mockSwitch) {
      console.log('mock-getWeeklyActivityStatus')
      return new Promise ((resolve, reject) => {
        resolve(mockData.getWeeklyActivityStatus)
      })
    } else {
      return http.get('getWeeklyActivityStatus')
              .then(data => {
                  return Promise.resolve(data)
              })
              .catch(err => {
                  return Promise.reject(err)
              })
    }
  },
  //获取每日活动状态
  getActivityStatus () {
    if (mockSwitch) {
      console.log('mock-getActivityStatus')
      return new Promise ((resolve, reject) => {
        resolve(mockData.getActivityStatus)
      })
    } else {
      return http.get('getActivityStatus')
              .then(data => {
                  return Promise.resolve(data)
              })
              .catch(err => {
                  return Promise.reject(err)
              })
    }
  },
  //抽奖
  draw () {
    if (mockSwitch) {
      console.log('mock-draw')
      return new Promise ((resolve, reject) => {
        resolve(mockData.draw)
      })
    } else {
      let data = {
        userId: sessionStorage.getItem('_userId')
      }
      return http.post('luckyDraw', data)
              .then(data => {
                  return Promise.resolve(data)
              })
              .catch(err => {
                  return Promise.reject(err)
              })
    }
  },
  //提交游戏结果
  submitResult (timeUsed) {
    if (mockSwitch) {
      console.log('mock-submitResult')
      return new Promise ((resolve, reject) => {
        resolve(mockData.submitResult)
      })
    } else {
      let data = {
        userId: sessionStorage.getItem('_userId'),
        timeUsed
      }
      return http.post('submitResult', data)
              .then(data => {
                  return Promise.resolve(data)
              })
              .catch(err => {
                  return Promise.reject(err)
              })
    }
  },
  //获取我的奖品列表
  getMyPrize () {
    if (mockSwitch) {
      console.log('mock-getMyPrize')
      return new Promise ((resolve, reject) => {
        resolve(mockData.getMyPrize)
      })
    } else {
      let data = {
        userId: sessionStorage.getItem('_userId')
      }
      return http.post('myPrize', data)
              .then(data => {
                  return Promise.resolve(data)
              })
              .catch(err => {
                  return Promise.reject(err)
              })
    }
  },
  //填写手机号
  submitPhone (phone) {
    if (mockSwitch) {
      console.log('mock-submitPhone')
      return new Promise ((resolve, reject) => {
        resolve(mockData.submitPhone)
      })
    } else {
      let data = {
        userId: sessionStorage.getItem('_userId'),
        phone
      }
      return http.post('submitPhone', data)
              .then(data => {
                  return Promise.resolve(data)
              })
              .catch(err => {
                  return Promise.reject(err)
              })
    }
  },
  //查看排行榜
  getRankingList (phone) {
    if (mockSwitch) {
      console.log('mock-getRankingList')
      return new Promise ((resolve, reject) => {
        resolve(mockData.getRankingList)
      })
    } else {
      let data = {
        userId: sessionStorage.getItem('_userId'),
        nickname: sessionStorage.getItem('_nickname')
      }
      return http.post('rankingList', data)
              .then(data => {
                  return Promise.resolve(data)
              })
              .catch(err => {
                  return Promise.reject(err)
              })
    }
  }
}

export default {
  install (Vue) {
    Vue.mixin({
      created () {
        this.$_api = api
      }
    })
  }
}
