export default {
  template: `
    <div class="app">
      <transition name="fade">
        <router-view/>
      </transition>  
      <div>
        <a :href="bannerUrl"><img :src="bannerImg" alt="" style="display:block;width: 100%;"></a>
        <img :src="copyrightImg" alt="" style="display:block;width: 100%;">
      </div>  
    </div>
  `,
  name: 'App',
  data () {
    return {
      bannerUrl: GLOBAL_CONFIG.commonImg.bannerUrl,
      bannerImg: GLOBAL_CONFIG.commonImg.bannerImg,
      copyrightImg: GLOBAL_CONFIG.commonImg.copyrightImg
    }
  }
}