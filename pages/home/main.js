import Vue from 'vue/dist/vue.min.js';
import router from './router';
import FastClick from 'fastclick'
import installAllPlugins from './plugins/installAll'
import registerAllComponents from './components/registerAll'
import api from './ajax/api'
import tools from './libs/tools'
// import VConsole from 'vconsole/dist/vconsole.min.js' //import vconsole
// let vConsole = new VConsole() // 初始化

FastClick.attach(document.body);

installAllPlugins(Vue)
registerAllComponents(Vue)
Vue.use(api)
Vue.use(tools)

new Vue({
  el: '#app',
  delimiters: ['[[', ']]'],
  router,
  data: {

  },
  created () {
    //获取支付宝授权信息
    // fdAlipayAuth.loadUserInfo({
    //   // fopcors: true,
    //   debugUserInfo: {
    //     userId: '208812345680',
    //     headImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl5449vh/b68b9d08-a1ea-4b7b-a9d9-436b29358e13_w180_h179.png',
    //     nickName: '支付宝用户666',
    //   }
    // })
    // .then(data => {
    //   console.log('支付宝授权信息: ',data)
    //   sessionStorage.setItem('_userId', data.userId)
    //   sessionStorage.setItem('_nickname', data.nickName)
    //   sessionStorage.setItem('_headImgUrl', data.headImgUrl)
    // })
    // .catch(err => {
    //   alert('授权失败')
    //   console.log(err)
    // })
    this.$_api.getActivityStatus()
    .then(data => {
      if (data.activityStatus ===1) {
        location.href = 'https://activity.cdollar.cn/p/q/jje1ulrb/pages/home/index.html?action=close'
      }
    })
    .catch(err => {
      console.log(err)
    })
    //获取全局图片url，用于图片预加载
    this.$_api.getCommonConfig()
    .then(data => {
        window.GLOBAL_CONFIG = data
        console.info('全局的后台配置项：', window.GLOBAL_CONFIG);
    })
    .catch(err => {
        console.log(err)
        alert('活动太火爆，请刷新再试试吧！')
    })

  }
})

console.info('全局的凤蝶配置项：', globalSchemaData);
