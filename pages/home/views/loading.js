const mockSwitch = globalSchemaData.dev.mockSwitch

export default {
    template: `
    <div id="loading" class="loading">
        <div class="loading_container" >
            <div class="loading_bar">
                <div class="loading_progress" :style="{'width': loadingPecent+'%'}">
                </div>
            </div>
            <div class="loading_text">
                {{Math.round(loadingPecent)}}%
            </div>
        </div>
    </div>
    `,
    name: 'Loading',
    data() {
        return {
            loadingPecent: 0,
            imgsPecent: 100, // 图片加载占比
            loadedimages: 0, // 已加载完成的图片数量
            firstPageImgs: []
        }
    },
    // 数据相关
    computed: {
        // 其它占比
        otherPercent: function () {
            return 100 - this.imgsPecent
        },
        // 每张图片加载占比
        imgUnit: function () {
            return this.imgsPecent / this.firstPageImgs.length
        },
    },
    watch: {
        loadingPecent: function (newVal) {
            if (Math.round(newVal) >= 100) {
                this.loaded()
            }
        }
    },
    // 生命周期
    created () {
        Object.keys(GLOBAL_CONFIG.commonImg).forEach(item => {
            if (item !== 'urlHome2More' && item !== 'bannerUrl') {
                this.firstPageImgs.push(GLOBAL_CONFIG.commonImg[item])
            } else {
                console.log(item)
            }
        })
        // console.log(this.firstPageImgs)
        this.loadFirstPage()
    },
    // 函数方法
    methods: {
        // 资源加载完之后的回调
        loaded() {
            console.log('loaded!')
            setTimeout(() => {
                this.$router.replace('/home')
            }, 100);
        },
        // 每张图片加载完成(成功或者失败)之后执行的回调
        imageloadpost () {
            this.loadingPecent += this.imgUnit
            // this.loadedimages++
            // setTimeout(() => {
            //     this.loadingPecent += this.imgUnit
            // }, 20 * this.loadedimages)
        },
        // 首页加载
        loadFirstPage () {
            // 首页图片预加载
            if (!this.firstPageImgs.length) return this.emptyLoading()
            for (let i = 0; i < this.firstPageImgs.length; i++) {
                const newImage = new Image()
                newImage.src = this.firstPageImgs[i]
                newImage.onload = () => {
                    this.imageloadpost()
                }
                newImage.onerror = () => {
                    this.imageloadpost()
                }
            }
        },
        // 空loading
        emptyLoading (step = 0) {
            console.log('there is no img to be preload')
            step++
            if (this.loadingPecent >= 100) return
            setTimeout(() => {
                this.loadingPecent += 5
                this.emptyLoading(step)
            }, 2 * step)
        }
    }
}
