import BScroll from 'better-scroll'

export default {
  template: `
    <div id="award" :style="'backgroundImage: url(' + awardBg +')'">
      <img class="award-title" :src="awardImgTitle" alt="awardImgTitle" />
      <div class="award-status" :style="'backgroundImage: url(' + awardBgStatus +')'">
        <div class="award-status-title">
          <div class="award-status-title-award">获得奖品</div>
          <div class="award-status-title-reason">获得原因</div>
          <div class="award-status-title-status">发放状态</div>
        </div>
        <div class="award-status-wrapper" ref="wrapper">
          <div>
            <AwardItem
              class="award-status-wrapper-status-item"
              v-for="(item,index) in awardArr"
              :key="index"
              :award="item.award"
              :reasonLine1="item.reasonLine1"
              :reasonLine2="item.reasonLine2"
              :status="item.status"
            />
          </div>
        </div>
      </div>
      <img v-if="isShowBtn" class="award-btn" :src="awardBtn" alt="awardBtn" @click="clickBtn"/>
      <img class="award-rule" :src="awardImgRule" alt="awardImgRule" />
      <MsgBox
        v-if="isShowMsgBox"
        :type="msgBoxType"
        :text="msgBoxText"
        @msgBoxClose="isShowMsgBox=false"
        @msgBoxConfirm="msgBoxConfirm"
        @msgBox2Confirm="msgBox2Confirm"
        @msgBox2Cancel="isShowMsgBox=false"
      />
    </div>
  `,
  name: 'Award',
  data () {
    return {
      //图片
      awardBg: GLOBAL_CONFIG.commonImg.awardBg,
      awardImgTitle: GLOBAL_CONFIG.commonImg.awardImgTitle,
      awardBgStatus: GLOBAL_CONFIG.commonImg.awardBgStatus,
      awardBtn: GLOBAL_CONFIG.commonImg.awardBtn,
      awardImgRule: GLOBAL_CONFIG.commonImg.awardImgRule,

      isShowMsgBox: false,
      msgBoxType: null,
      retryTimes: 0,
      msgBoxText: null,

      isShowBtn: false,

      awardArr: []
    }
  },
  created () {
    this.getMyPrize()
  },
  mounted () {
    // this.$nextTick(() => {
    //   this.scroll = new BScroll(this.$refs.wrapper, {
    //     scrollbar: {
    //       fade: false,
    //       interactive: true
    //     }
    //   })
    // })
  },
  methods: {
    clickBtn () {
      this.msgBoxType = 'PHONE_INPUT'
      this.isShowMsgBox = true
    },
    msgBoxConfirm (val) {
      this.isShowMsgBox = false
      console.log(val)
      if (this.msgBoxType === 'PHONE_INPUT')
      this.submitPhone(val.phone)
    },
    msgBox2Confirm () {
      this.isShowMsgBox = false
      this.retryTimes ++
      this.getMyPrize()
    },
    getMyPrize () {
      this.$_indicator.show()
      this.$_api.getMyPrize()
      .then(data => {
        this.$_indicator.close()
        console.log(data)
        this.isShowBtn = data.phoneStatus === 1
        data.myPrize.forEach(item => {
          this.awardArr.push({
            award: item.prizeName,
            reasonLine1: item.type === 0 ? `${this.$_tools.dateNum2dateObj(item.date).month+this.$_tools.dateNum2dateObj(item.date).date}排行榜` : this.$_tools.dateNum2dateObj(item.date).month+this.$_tools.dateNum2dateObj(item.date).date,
            reasonLine2: item.type === 0 ? `第${item.rank}名` : '幸运星期二抽奖',
            status: (status => {
              switch (status) {
                case -1: return '待联系'
                case 0: return '发奖失败'
                case 1: return '已发奖'
                default: return ''
              }
            })(item.status)
          })
        })
        if (this.awardArr.length < 6) {
          let size = 6-this.awardArr.length
          for (let i=0; i<size; i++) {
            this.awardArr.push({})
          }
        }
      })
      .catch(err => {
        this.$_indicator.close()
        console.log(err)
        if (this.retryTimes > 2) {
          this.msgBoxType = 'DRAW_TEXT'
          this.isShowMsgBox = true
          this.msgBoxText = '请稍后再试'
          this.msgBoxType = 'DRAW_TEXT'
          this.isShowMsgBox = true
        } else {
          this.msgBoxType = 'MSG_CONFIRM'
          this.msgBoxText = '数据获取失败'
          this.isShowMsgBox = true
        }
      })
    },
    submitPhone (phone) {
      this.$_indicator.show()
      this.$_api.submitPhone(phone)
      .then(data => {
        this.$_indicator.close()
        console.log(data)
        let text
        switch (data.code) {
          case 1: text = '提交成功';this.isShowBtn = false;break
          case -1: text = '没有中奖，无需填写';break
          case 0: text = '提交失败';break
          default: ;
        }
        setTimeout(() => {
          this.$_toast.show(text)
        }, 500)
      })
      .catch(err => {
        this.$_indicator.close()
        console.log(err)
        setTimeout(() => {
          this.$_toast.show('提交失败')
        }, 500)
      })
    }
  }
}
