export default {
  template: `
    <div id="home" :style="'backgroundImage: url(' + homeBg +')'">
      <MarqueePause class="marquee-pause" :arr="marqueeData"/>
      <div class="my-award-btn" :style="'backgroundImage: url(' + homeBtnMyAward +')'" @click="$router.push('/award')">
        <div v-if="isShowNewLabel" class="award-label">{{prizeNum}}</div>
        <img v-else-if="isShowBigNewLabel" class="award-big-label" :src="homeImgRedPoint" alt="homeImgRedPoint" />
      </div>
      <img class="titleImg" :src="homeImgTitle" alt="homeImgTitle" />
      <a :href="urlHome2More"><img class="gift" :src="homeBtnGift" alt="homeBtnGift" /></a>
      <div class="btn-start-game" :style="'backgroundImage: url(' + homeBtnStart +')'" @click="$router.push('/game')"></div>
      <div class="btn-draw" :style="'backgroundImage: url(' + homeBtnDraw +')'" @click="clickDraw"></div>
      <div class="btn-Rank" :style="'backgroundImage: url(' + homeBtnRank +')'" @click="$router.push('/rank')"></div>
      <img class="rule" :src="homeImgRule" alt="homeImgRule" />
      <MsgBox
        v-if="(isShowMsgBoxTips && !isHadShowMsgBox && isDrawOpen && !isAlreadyDraw) || isShowMsgBox"
        :type="msgBoxType"
        :bigPrizeName="bigPrizeName"
        :bigPrizeImgUrl="bigPrizeImgUrl"
        :text="msgBoxText"
        @msgBoxClose="msgBoxClose"
        @msgBoxConfirm="msgBoxConfirm"
        @msgBox2Confirm="msgBox2Confirm"
        @msgBox2Cancel="isShowMsgBox=false"
      />
    </div>
  `,
  name: 'Home',
  data () {
    return {
      //图片
      homeBg: GLOBAL_CONFIG.commonImg.homeBg,
      homeImgTitle: GLOBAL_CONFIG.commonImg.homeImgTitle,
      homeImgRule: GLOBAL_CONFIG.commonImg.homeImgRule,
      homeBtnGift: GLOBAL_CONFIG.commonImg.homeBtnGift,
      homeBtnMyAward: GLOBAL_CONFIG.commonImg.homeBtnMyAward,
      homeBtnRank: GLOBAL_CONFIG.commonImg.homeBtnRank,
      homeBtnDraw: GLOBAL_CONFIG.commonImg.homeBtnDraw,
      homeBtnStart: GLOBAL_CONFIG.commonImg.homeBtnStart,
      homeImgRedPoint: GLOBAL_CONFIG.commonImg.homeImgRedPoint,
      //查看更多的url
      urlHome2More: GLOBAL_CONFIG.commonImg.urlHome2More,

      marqueeData: [],

      isShowNewLabel: false,
      prizeNum: null,
      isShowBigNewLabel: false,

      isShowMsgBoxTips: false,
      isShowMsgBox: false,
      isHadShowMsgBox: false,
      msgBoxType: null,
      msgBoxText: null,
      isAlreadyDraw: false,

      bigPrizeName: null,
      bigPrizeImgUrl: null,

      isDrawOpen: false,

      retryTimes: 0
    }
  },
  created () {
    //限制只弹出一次
    if (sessionStorage.getItem('_isHadShowMsgBox')) {
      this.isHadShowMsgBox = true
    }
    let that = this;
    fdAlipayAuth.loadUserInfo({
      // fopcors: true,
      debugUserInfo: {
        userId: '208812345680',
        headImgUrl: 'https://gw.alipayobjects.com/os/q/cms/images/jl5449vh/b68b9d08-a1ea-4b7b-a9d9-436b29358e13_w180_h179.png',
        nickName: '支付宝用户666',
      }
    })
    .then(data => {
      console.log('支付宝授权信息: ',data)
      sessionStorage.setItem('_userId', data.userId)
      sessionStorage.setItem('_nickname', data.nickName)
      sessionStorage.setItem('_headImgUrl', data.headImgUrl)
      that.getIndexInfo()
    })
    .catch(err => {
      alert('授权失败')
      console.log(err)
    })
    
  },
  mounted () {
    this.getWeeklyActivityStatus()
  },
  methods: {
    msgBoxConfirm (val) {
      this.isShowMsgBox = false
      console.log(val)
      if (this.isDrawOpen) {
        sessionStorage.setItem('_isHadShowMsgBox', 'true')
        this.$router.push('/draw')
      }
    },
    msgBox2Confirm () {
      this.isShowMsgBox = false
      this.retryTimes ++
      this.getIndexInfo()
    },
    msgBoxClose () {
      if (this.isDrawOpen) {
        sessionStorage.setItem('_isHadShowMsgBox', 'true')
      }
      this.isShowMsgBoxTips = false
      this.isShowMsgBox = false
    },
    getIndexInfo () {
      this.$_indicator.show()
      console.log('开始拿缓存啦')
      this.$_api.getIndexInfo()
      .then(data => {
        this.$_indicator.close()
        console.log(data)
        if (data.luckyPrize.alreadyDraw === 1) {
          this.isAlreadyDraw = true
        }
        if (data.luckyPrize) {
          this.bigPrizeName = data.luckyPrize.prizeName
          this.bigPrizeImgUrl = data.luckyPrize.prizeImgUrl
        }
        this.marqueeData = data.recentPrizePeople
        sessionStorage.setItem('_marqueeData', JSON.stringify(data.recentPrizePeople))
        if (data.newPrizeStatus === -1) {
          this.isShowBigNewLabel = true
        } else if (data.newPrizeStatus > 0) {
          this.isShowNewLabel = true
          this.prizeNum = data.newPrizeStatus
        }
      })
      .catch(err => {
        this.$_indicator.close()
        console.log(err)
        if (this.retryTimes > 2) {
          this.msgBoxType = 'DRAW_TEXT'
          this.msgBoxText = '请稍后再试'
          this.isShowMsgBox = true
        } else {
          this.msgBoxType = 'MSG_CONFIRM'
          this.msgBoxText = '数据获取失败'
          this.isShowMsgBox = true
        }
      })
    },
    getWeeklyActivityStatus () {
      this.$_api.getWeeklyActivityStatus()
      .then(data => {
        console.log(data)
        if (data.activityStatus === 0) {
          this.isDrawOpen = true
          setTimeout(() => {
            this.msgBoxType = 'DRAW_TIPS'
            this.isShowMsgBoxTips = true
          }, 500)
        }
      })
      .catch(err => {
        console.log(err)
      })
    },
    clickDraw () {
      if (this.isDrawOpen) {
        this.$router.push('/draw')
      } else {
        this.msgBoxType = 'DRAW_ENTRY_DISABLE'
        this.isShowMsgBox = true
      }

    }
  }
}
