import BScroll from 'better-scroll'

export default {
  template: `
    <div id="rank" :style="'backgroundImage: url(' + rankBg +')'">
      <img class="rank-title" :src="rankImgTitle" alt="rankImgTitle" />
      <img class="rank-explain" :src="rankBgExplain" alt="rankBgExplain" />
      <div class="rank-self" :style="'backgroundImage: url(' + rankBgSelf +')'">
        <p class="rank-self-title">{{rankSelfTitle}}</p>
        <div class="rank-self-content">
          <div class="rank-self-content-short">{{myRank}}</div>
          <div class="rank-self-content-long">我自己</div>
          <div class="rank-self-content-long">{{myTime}}s</div>
        </div>
      </div>
      <div class="rank-all" :style="'backgroundImage: url(' + rankBgAll +')'">
        <div class="rank-all-title">
          <div>排名</div>
          <div>用户名</div>
          <div>成绩</div>
        </div>
        <div class="rank-all-wrapper" ref="wrapper">
          <div>
            <RankItem
              class="rank-all-wrapper-rank-item"
              v-for="(item,index) in rankArr"
              :key="index"
              :rankNum="index"
              :userName="item.nickName"
              :result="item.time"
            />
          </div>
        </div>
      </div>
      <MsgBox
        v-if="isShowMsgBox"
        :type="msgBoxType"
        :text="msgBoxText"
        @msgBoxClose="isShowMsgBox=false"
        @msgBoxConfirm="isShowMsgBox=false"
        @msgBox2Confirm="msgBox2Confirm"
        @msgBox2Cancel="isShowMsgBox=false"
      />
    </div>
  `,
  name: 'Rank',
  data () {
    return {
      //图片
      rankBg: GLOBAL_CONFIG.commonImg.rankBg,
      rankImgTitle: GLOBAL_CONFIG.commonImg.rankImgTitle,
      rankBgExplain: GLOBAL_CONFIG.commonImg.rankBgExplain,
      rankBgSelf: GLOBAL_CONFIG.commonImg.rankBgSelf,
      rankBgAll: GLOBAL_CONFIG.commonImg.rankBgAll,

      myRank: null,
      myTime: null,
      rankArr: [],
      isInRank: false,

      isShowMsgBox: false,
      msgBoxType: null,
      retryTimes: 0,
      msgBoxText: null,
    }
  },
  computed: {
    rankSelfTitle () {
      return this.isInRank ? '你已经榜上有名了，果然厉害!' : '再加把劲就能拿到奖品啦～'
    }
  },
  created () {
    this.getRankingList()
  },
  mounted () {
    // this.$nextTick(() => {
    //   this.scroll = new BScroll(this.$refs.wrapper, {
    //     scrollbar: {
    //       fade: false,
    //       interactive: true
    //     }
    //   })
    // })
  },
  methods: {
    msgBox2Confirm () {
      this.isShowMsgBox = false
      this.retryTimes ++
      this.getRankingList()
    },
    getRankingList () {
      this.$_indicator.show()
      this.$_api.getRankingList()
      .then(data => {
        this.$_indicator.close()
        console.log(data)
        if (data.rankingList.length === 0 || !data.myRank) {
          this.isInRank = false
        } else {
          this.isInRank = data.myRank <= data.rankingList.length
        }
        data.rankingList.forEach(item => {
          this.rankArr.push({
            nickName: item.nickName,
            time: (item.timeUsed).toFixed(2) + 's'
          })
        })
        if (this.rankArr.length < 10) {
          let size = 10-this.rankArr.length
          for (let i=0; i<size; i++) {
            this.rankArr.push({})
          }
        }
        this.myRank = data.myRank ? data.myRank : '----'
        this.myTime = data.myTime ? (data.myTime).toFixed(2) : '--.-- '
      })
      .catch(err => {
        this.$_indicator.close()
        console.log(err)
        if (this.retryTimes > 2) {
          this.msgBoxType = 'DRAW_TEXT'
          this.isShowMsgBox = true
          this.msgBoxText = '请稍后再试'
          this.msgBoxType = 'DRAW_TEXT'
          this.isShowMsgBox = true
        } else {
          this.msgBoxType = 'MSG_CONFIRM'
          this.msgBoxText = '数据获取失败'
          this.isShowMsgBox = true
        }
      })
    }
  }
}
