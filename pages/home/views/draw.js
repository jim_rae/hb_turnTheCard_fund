export default {
  template: `
    <div id="draw" :style="'backgroundImage: url(' + drawBg +')'">
      <MarqueePause class="marquee-pause" :arr="marqueeData"/>
      <div class="my-award-btn" :style="'backgroundImage: url(' + homeBtnMyAward +')'" @click="$router.push('/award')">
        <div v-if="isShowNewLabel" class="award-label">{{prizeNum}}</div>
        <img v-else-if="isShowBigNewLabel" class="award-big-label" :src="homeImgRedPoint" alt="homeImgRedPoint" />
      </div>
      <img class="draw-title" :src="drawImgTitle" alt="drawImgTitle" />
      <img class="draw-award" :src="drawImgAward" alt="drawImgAward" />
      <img class="draw-btn" :src="drawBtn" alt="drawBtn" @click="clickDraw"/>
      <MsgBox
        v-if="isShowMsgBox"
        :type="msgBoxType"
        :getPrizeName="getPrizeName"
        :getPrizeImgUrl="getPrizeImgUrl"
        :text="msgBoxText"
        @msgBoxClose="isShowMsgBox=false"
        @msgBoxConfirm="msgBoxConfirm"
        @msgBox2Confirm="msgBox2Confirm"
        @msgBox2Cancel="isShowMsgBox=false"
      />
    </div>
  `,
  name: 'Draw',
  data () {
    return {
      //图片
      drawBg: GLOBAL_CONFIG.commonImg.drawBg,
      drawImgTitle: GLOBAL_CONFIG.commonImg.drawImgTitle,
      drawImgAward: GLOBAL_CONFIG.commonImg.drawImgAward,
      drawBtn: GLOBAL_CONFIG.commonImg.drawBtn,
      homeBtnMyAward: GLOBAL_CONFIG.commonImg.homeBtnMyAward,
      homeImgRedPoint: GLOBAL_CONFIG.commonImg.homeImgRedPoint,

      marqueeData: [],

      isShowAwardLabel: true,

      isShowMsgBox: false,
      msgBoxType: null,
      getPrizeName: null,
      getPrizeImgUrl: null,
      msgBoxText: null,

      isShowNewLabel: false,
      isShowBigNewLabel: false,
      prizeNum: null,

      retryTimes: 0
    }
  },
  created () {
    this.getWeeklyActivityStatus()
    this.getIndexInfo()
  },
  methods: {
    msgBoxConfirm () {
      this.isShowMsgBox = false
      if (this.msgBoxType === 'DRAW_AWARD') {
        this.getIndexInfo()
      }
    },
    msgBox2Confirm () {
      this.isShowMsgBox = false
      this.retryTimes ++
      this.getIndexInfo()
    },
    getWeeklyActivityStatus () {
      this.$_api.getWeeklyActivityStatus()
      .then(data => {
        console.log(data)
        if (data.activityStatus !== 0) {
          this.$router.replace('/home')
        }
      })
      .catch(err => {
        console.log(err)
      })
    },
    clickDraw () {
      this.draw()
    },
    draw () {
      this.$_indicator.show()
      this.$_api.draw()
      .then(data => {
        this.$_indicator.close()
        console.log(data)
        if (data.code === 1) {
          this.msgBoxType = 'DRAW_AWARD'
          this.getPrizeName = data.prizeName
          this.getPrizeImgUrl = data.prizeImgUrl
          this.isShowMsgBox = true
        } else if (data.code === 2) {
          this.msgBoxType = 'DRAW_TEXT'
          this.msgBoxText = '您已参与过抽奖，请下周再来吧～'
          this.isShowMsgBox = true
        }
      })
      .catch(err => {
        this.$_indicator.close()
        console.log(err)
        this.msgBoxType = 'DRAW_TEXT'
        this.msgBoxText = '数据获取失败'
        this.isShowMsgBox = true
      })
    },
    getIndexInfo () {
      this.$_indicator.show()
      this.$_api.getIndexInfo()
      .then(data => {
        this.$_indicator.close()
        console.log(data)
        if (data.luckyPrize) {
          this.bigPrizeName = data.luckyPrize.prizeName
          this.bigPrizeImgUrl = data.luckyPrize.prizeImgUrl
        }
        this.marqueeData = data.recentPrizePeople
        sessionStorage.setItem('_marqueeData', JSON.stringify(data.recentPrizePeople))
        if (data.newPrizeStatus === -1) {
          this.isShowBigNewLabel = true
        } else if (data.newPrizeStatus > 0) {
          this.isShowNewLabel = true
          this.prizeNum = data.newPrizeStatus
        }
      })
      .catch(err => {
        this.$_indicator.close()
        console.log(err)
        if (this.retryTimes > 2) {
          this.msgBoxType = 'DRAW_TEXT'
          this.msgBoxText = '请稍后再试'
          this.isShowMsgBox = true
        } else {
          this.msgBoxType = 'MSG_CONFIRM'
          this.msgBoxText = '数据获取失败'
          this.isShowMsgBox = true
        }
      })
    }
  }
}
