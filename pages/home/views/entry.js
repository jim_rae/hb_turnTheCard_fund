import Loading from './loading'
import Home from './home'
import Game from './game'
import Rank from './rank'
import Award from './award'
import Draw from './draw'

export default {
    Loading,
    Home,
    Game,
    Rank,
    Award,
    Draw
}
