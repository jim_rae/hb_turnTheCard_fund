export default {
  template: `
    <div id="game" :style="'backgroundImage: url(' + gameBg +')'" @touchmove.prevent>
      <MarqueePause class="marquee-pause" :arr="marqueeData"/>
      <div class="title" :style="'backgroundImage: url(' + gameBgRedTie +')'">第{{levelInfo.level}}关</div>
      <div class="time-container" :style="'backgroundImage: url(' + gameBgTime +')'">
        <p class="time-container-time">剩余：{{time}}s</p>
        <div class="progress-wrapper">
          <img :class="['time-container-progress', {animate: isCountDown}]" :src="gameImgProgress" alt="gameImgProgress" ref="progress" :style="hideCountDown"/>
        </div>
        <img class="time-container-clock" :src="gameImgClock" alt="gameImgClock" />
      </div>
      <div class="game-container" :style="'backgroundImage: url(' + gameBgInner +')'">
        <Card
          v-for="(item, index) in cardArr"
          ref="card"
          :key="index"
          :class="{'game-container-card-level1': cardArrIndex===0, 'game-container-card-level2': cardArrIndex===1, 'game-container-card-level3': cardArrIndex===2}"
          :isShow="item.isShow"
          :cardFrontImg="item.fundImgUrl"
          :isOpen="item.isOpen"
          @click.native="clickCard(index)"
        />
      </div>
      <MsgBox
        v-if="isShowMsgBox"
        :type="msgBoxType"
        :curTime="curTime"
        :bestTime="bestTime"
        :text="msgBoxText"
        :isShowNewLabel="isShowNewLabel"
        @msgBoxClose="isShowMsgBox=false"
        @msgBoxConfirm="msgBoxConfirm"
        @msgBox2Confirm="msgBox2Confirm"
        @msgBox2Cancel="isShowMsgBox=false"
      />
      <MsgBoxIntro
        v-if="isShowMsgBoxIntro"
        :fundInfo="msgBoxIntroFundInfo"
        @msgBoxIntroClose="isShowMsgBoxIntro=false"
        @msgBoxIntroConfirm="msgBoxIntroConfirm"
      />
    </div>
  `,
  name: 'Game',
  data () {
    return {
      //图片
      gameBgRedTie: GLOBAL_CONFIG.commonImg.gameBgRedTie,
      gameBgTime: GLOBAL_CONFIG.commonImg.gameBgTime,
      gameImgProgress: GLOBAL_CONFIG.commonImg.gameImgProgress,
      gameImgClock: GLOBAL_CONFIG.commonImg.gameImgClock,
      gameBgInner: GLOBAL_CONFIG.commonImg.gameBgInner,
      gameBg: GLOBAL_CONFIG.commonImg.gameBg,
      logoBaisheng: GLOBAL_CONFIG.commonImg.logoBaisheng,

      cardArrIndex: 0,

      marqueeData: [],

      time: null,
      clickIndexArr: [],

      isShowMsgBox: false,
      msgBoxType: null,
      curTime: null,
      bestTime: '--',
      isShowNewLabel: false,
      msgBoxText: null,
      retryTimes: 0,

      timer: null,

      isShowMsgBoxIntro: false,

      isAnimating: false,

      transitionendTime: 0,

      isCountDown: false,

      hideCountDown: '',//用来隐藏游戏失败之后的计时条
    }
  },
  watch: {
    clickIndexArr (newVal) {
      if (newVal.length === 2) {
        console.log(this.cardArr[newVal[0]].fundName)
        console.log(this.cardArr[newVal[1]].fundName)
        if (this.cardArr[newVal[0]].fundName === this.cardArr[newVal[1]].fundName) {
          setTimeout(() => {
            this.cardArr[newVal[0]].isShow = false
            this.cardArr[newVal[1]].isShow = false
            this.transitionendTime += 2
            if (this.cardArr.every(item => {return !item.isShow})) {
              this.isCountDown = false
              if (this.cardArrIndex === 2) {
                this.submitResult()
              } else {
                clearInterval(this.timer)
                this.msgBoxType = 'GAME_WIN'
                this.isShowMsgBox = true
                this.hideCountDown = 'opacity:0;'
              }
            }
          }, 500)
        } else {
          setTimeout(() => {
            this.cardArr[newVal[0]].isOpen = false
            this.cardArr[newVal[1]].isOpen = false
          }, 500)
        }
        this.clickIndexArr = []
      }
    }
  },
  computed: {
    levelInfo () {
      return GLOBAL_CONFIG.levelInfo[this.cardArrIndex]
    },
    cardArr () {
      var arr = []
      GLOBAL_CONFIG.levelInfo[this.cardArrIndex].fund.forEach(item => {
        for (let i=0; i<item.times; i++) {
          arr.push({
            isShow: false,
            isOpen: false,
            fundName: item.fundName,
            fundImgUrl: item.fundImgUrl
          })
        }
      })
      arr = this.$_tools.shuffle(arr)
      switch (this.cardArrIndex) {
        case 0: arr.splice(4, 0, {
          isShow: false
        }); break
        case 2: arr.splice(12, 0, {
          isShow: false
        }); break
        default: ;
      }
      return arr
    },
    msgBoxIntroFundInfo () {
      if (this.cardArrIndex === 0) {
        return this.levelInfo.fund
      } else {
        return this.levelInfo.fund.slice(GLOBAL_CONFIG.levelInfo[this.cardArrIndex-1].fund.length)
      }
    }
  },
  created () {
    this.marqueeData = JSON.parse(sessionStorage.getItem('_marqueeData'))
  },
  mounted () {
    setTimeout(() => {
      this.isShowMsgBoxIntro = true
    },500)
  },
  beforeDestroy () {
    clearInterval(this.timer)
  },
  methods: {
    start () {
      // this.$refs.card.forEach((item, index) => {
      //   item.$el.addEventListener(this.$_tools.whichAnimationEvent(), this.animationFun)
      // })
      //设置牌子信息
      this.cardArr.forEach((item, index) => {
        item.isOpen = false
        switch (this.cardArrIndex) {
          case 0: {
            if (index !== 4) {
              item.isShow = true
            }
          }; break
          case 2: {
            if (index !== 12) {
              item.isShow = true
            }
          }; break
          default: item.isShow = true;
        }
      })
      this.isAnimating = true
      setTimeout(() => {
        this.isAnimating = false
      },1000)
      //设置时间
      this.time = GLOBAL_CONFIG.levelInfo[this.cardArrIndex].time
      this.$refs.progress.style.animationDuration = this.time + 's'
      this.$refs.progress.style.webkitAnimationDuration = this.time + 's'
      this.isCountDown = true
      //开始倒计时
      this.timer = setInterval(() => {
        if (this.time === '0.00') {
          clearInterval(this.timer)
          this.msgBoxType = 'GAME_LOSE'
          this.isShowMsgBox = true
          this.isCountDown = false
          this.hideCountDown = 'opacity:0;'
        } else {
          this.time = (this.time - 0.01).toFixed(2)
        }
      }, 10)
    },
    animationFun () {
      console.log('animationend')
      this.isAnimating = false
    },
    clickCard (index) {
      if (this.cardArr[index].isShow && !this.cardArr[index].isOpen && !this.isAnimating) {
        this.cardArr[index].isOpen = true
        this.clickIndexArr.push(index)
      }
    },
    msgBoxConfirm (val) {
      console.log(val)
      // this.$refs.card.forEach((item, index) => {
      //   item.$el.removeEventListener(this.$_tools.whichAnimationEvent(), this.animationFun)
      // })
      this.cardArr.forEach(item => {
        item.isShow = false
      })
      switch (val.type) {
        case 'GAME_LOSE': {
          this.cardArrIndex = 0
          this.isShowMsgBoxIntro = true
          this.hideCountDown = 'opacity:1;'
        }; break
        case 'GAME_WIN': {
          if (this.cardArrIndex < 2) {
            this.cardArrIndex += 1
            this.isShowMsgBoxIntro = true
            this.hideCountDown = 'opacity:1;'
          }
        }; break
        case 'GMAE_RECORD': {
          this.$router.replace('/rank')
        }; break
        default: ;
      }
    },
    msgBoxIntroConfirm () {
      setTimeout(() => {
        this.start()
      }, 500)
    },
    msgBox2Confirm () {
      this.isShowMsgBox = false
      this.retryTimes ++
      this.submitResult()
    },
    submitResult () {
      clearInterval(this.timer)
      this.curTime = (GLOBAL_CONFIG.levelInfo[this.cardArrIndex].time - this.time).toFixed(2)
      this.$_indicator.show()
      this.$_api.submitResult(Number(this.curTime))
      .then(data => {
        this.$_indicator.close()
        console.log(data)
        if (data.newScoreStatus === 1) {
          this.isShowNewLabel = true
        }
        this.bestTime = (data.historyScore).toFixed(2)
        this.msgBoxType = 'GMAE_RECORD'
        this.isShowMsgBox = true
      })
      .catch(err => {
        this.$_indicator.close()
        console.log(err)
        if (this.retryTimes > 2) {
          this.msgBoxType = 'DRAW_TEXT'
          this.msgBoxText = '请稍后再试'
          this.isShowMsgBox = true
        } else {
          this.msgBoxType = 'MSG_CONFIRM'
          this.msgBoxText = '提交数据失败'
          this.isShowMsgBox = true
        }
      })
    }
  }
}
