/*
 * @Author: Jim
 * @Date: 2018-08-23
 * @Last Modified by: Jim
 * @Last Modified time: 2018-08-23
 * @desc toast提示窗
 */

const Toast = {
  template: `
    <transition name="indicator-fade">
      <div v-if="visible" class="toast">
        {{message}}
      </div>
    </transition>
  `,
  name: 'Toast',
  data () {
    return {
      message: null,
      visible: false
    }
  },
  methods: {
    show (msg) {
      this.message = msg
      this.visible = true
      setTimeout(() => {
        this.visible = false
      },3000)
    }
  }
}

export default {
  install (Vue) {
    const VueToast = Vue.extend(Toast)
    const $vm = new VueToast({
      el: document.createElement('div')
    })
    document.body.appendChild($vm.$el)

    const toast = {
      show (msg) {
        $vm.show(msg)
      }
    }

    Vue.mixin({
      created () {
        this.$_toast = toast
      }
    })
  }
}
