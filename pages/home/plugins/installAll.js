import Indicator from './indicator'
import Toast from './toast'

const plugins = {
  Indicator,
  Toast
}

export default function installAllPlugins (instance) {
  Object.keys(plugins).forEach(key => {
    instance.use(plugins[key])
  })
}
