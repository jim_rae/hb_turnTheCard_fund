export default {
  template: `
    <div class="rank-item">
      <img v-if="rankNum < 3 && userName" :src="rankImg" alt="rankImg" />
      <div v-if="userName" class="rank-item-num">{{rankNum+1}}</div>
      <div v-if="userName" class="rank-item-username">{{userName}}</div>
      <div v-if="userName" class="rank-item-result">{{result}}</div>
    </div>
  `,
  name: 'RankItem',
  props: {
    rankNum: {
      type: Number,
      required: true
    },
    userName: {
      type: String,
      required: true
    },
    result: {
      type: String,
      required: true
    }
  },
  computed: {
    rankImg () {
      switch (this.rankNum) {
        case 0: return GLOBAL_CONFIG.commonImg.rankImgGold; break
        case 1: return GLOBAL_CONFIG.commonImg.rankImgSilver; break
        case 2: return GLOBAL_CONFIG.commonImg.rankImgCopper; break
        default: ;
      }
    }
  }
}
