import BScroll from 'better-scroll'

export default {
  template: `
    <div class="msg-box-intro" @touchmove.prevent="">
      <transition
        name="custom-classes-transition"
        enter-active-class="animated zoomIn faster"
        leave-active-class="animated zoomOut faster"
      >
        <div v-if="isShow" class="msg-box-intro-wrapper" :style="'backgroundImage: url(' + msgboxBgLong +')'">
          <p class="msg-box-intro-wrapper-title">找出相同的牌子全部消除吧!</p>
          <div class="msg-box-intro-wrapper-marquee" :style="'backgroundImage: url(' + msgboxImgMarqueeBg +')'">
            <div class="hidden-wrapper">
              <div :class="['marquee-container', {'marquee-container-animation': isAnimated}]">
                <img v-for="item in marqueeArr" :src="item" alt="fundImg" />
              </div>
            </div>
          </div>
          <div class="msg-box-intro-wrapper-intro" ref="wrapper">
            <ul>
              <li v-for="item in fundInfo">
                <div class="intro-item" :style="'backgroundImage: url(' + msgboxBgIntro +')'">
                  <p class="intro-item-title">{{item.fundName}}</p>
                  <p class="intro-item-dec">{{item.fundIntroduce}}</p>
                </div>
              </li>
            </ul>
          </div>
          <div class="msg-box-intro-wrapper-btn" :style="'backgroundImage: url(' + msgboxBtnStart +')'" @click="clickStart"></div>
        </div>
      </transition>
    </div>
  `,
  name: 'MsgBoxIntro',
  props: {
    fundInfo: {
      type: Array,
      required: true
    }
  },
  data () {
    return {
      //图片
      msgboxBgLong: GLOBAL_CONFIG.commonImg.msgboxBgLong,
      msgboxBgIntro: GLOBAL_CONFIG.commonImg.msgboxBgIntro,
      msgboxBtnStart: GLOBAL_CONFIG.commonImg.msgboxBtnStart,
      msgboxImgMarqueeBg: GLOBAL_CONFIG.commonImg.msgboxImgMarqueeBg,

      isShow: false,
      isAnimated: false
    }
  },
  computed: {
    marqueeArr () {
      var arr = []
      this.fundInfo.forEach(item => {
        arr.push(item.fundImgUrl)
      })
      arr.push(this.fundInfo[0].fundImgUrl)
      arr.push(this.fundInfo[1].fundImgUrl)
      return arr
    },
    translateX () {
      return 195 * this.fundInfo.length / 200
    }
  },
  created () {
    //动态控制动画滚动距离
    var style = document.createElement('style')
    style.type = 'text/css'
    style.innerHTML = `
      @keyframes myMove {
        0% {
          transform: translateX(0);
        }
        100% {
          transform: translateX(-${this.translateX}rem);
        }
      }
      @-webkit-keyframes myMove {
        0% {
          -webkit-transform: translateX(0);
        }
        100% {
          -webkit-transform: translateX(-${this.translateX}rem);
        }
      }
    `
    document.getElementsByTagName('head')[0].appendChild(style)

    if (this.fundInfo.length > 2) {
      this.isAnimated = true
    }
  },
  mounted () {
    this.isShow = true
    this.$nextTick(() => {
      this.scroll = new BScroll(this.$refs.wrapper, {
        scrollbar: {
          fade: false,
          interactive: true
        }
      })
    })
  },
  methods: {
    close () {
      this.isShow = false
      setTimeout(() => {
        this.$emit('msgBoxIntroClose')
      }, 200)
    },
    clickStart () {
      this.$emit('msgBoxIntroConfirm')
      this.close()
    }
  }
}
