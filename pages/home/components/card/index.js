export default {
  template: `
  <div class="card-wrapper">
    <transition
      name="custom1-classes-transition"
      enter-active-class="animated bounceIn"
      leave-active-class="animated zoomOut"
    >
      <div v-if="isShow" :class="[isOpen ? 'card-front' : 'card-back', 'card']">
        <div class="cardBack" :style="'backgroundImage: url(' + gameImgCardBack +')'"></div>
        <div class="cardFront" :style="'backgroundImage: url(' + cardFrontImg +')'"></div>
      </div>
    </transition>
  </div>
  `,
  name: 'Card',
  props: {
    isShow: {
      type: Boolean,
      required: true
    },
    cardFrontImg: {
      type: String
    },
    isOpen: {
      type: Boolean
    }
  },
  data () {
    return {
      //图片
      gameImgCardBack: GLOBAL_CONFIG.commonImg.gameImgCardBack,
    }
  }
}
