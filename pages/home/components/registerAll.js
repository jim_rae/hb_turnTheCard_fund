import MarqueePause from './marquee-pause'
import MsgBox from './msg-box'
import MsgBoxIntro from './msg-box-intro'
import Card from './card'
import RankItem from './rank-item'
import AwardItem from './award-item'

const components = {
  MarqueePause,
  MsgBox,
  Card,
  MsgBoxIntro,
  RankItem,
  AwardItem
}

export default function registerAllComponents (instance) {
  Object.keys(components).forEach((key, index) => {
    instance.component(key, components[key])
  })
}
