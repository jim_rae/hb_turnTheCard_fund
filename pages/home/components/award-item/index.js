export default {
  template: `
    <div class="award-item">
      <div class="award-item-award">{{award}}</div>
      <div class="award-item-reason">
        <p>{{reasonLine1}}</p>
        <p>{{reasonLine2}}</p>
      </div>
      <div class="award-item-status">{{status}}</div>
    </div>
  `,
  name: 'AwardItem',
  props: {
    award: {
      type: String,
      required: true
    },
    reasonLine1: {
      type: String,
      required: true
    },
    reasonLine2: {
      type: String,
      required: true
    },
    status: {
      type: String,
      required: true
    }
  }
}
