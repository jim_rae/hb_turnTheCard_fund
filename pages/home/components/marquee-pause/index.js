export default {
  template: `
    <div class="marquee-pause-container">
      <transition class="inner-container" name="slide" mode="out-in">
        <p v-if="arr.length!==0" class="text" :key="text.id">恭喜<span class="name">{{text.val.nickname}}</span>获得了{{text.val.prizeName}}!</p>
      </transition>
    </div>
  `,
  name: 'MarqueePause',
  props: {
    arr: {
      type: Array,
      required: true,
      validator: function (value) {
        return value.length !== 0
      }
    }
  },
  data () {
    return {
      number: 0,
    }
  },
  computed: {
    text() {
      return {
        id: this.number,
        val: this.arr[this.number]
      }
    }
  },
  mounted() {
    this.startMove()
  },
  methods: {
    startMove() {
      let timer = setTimeout(() => {
        if (this.number === this.arr.length-1) {
          this.number = 0;
        } else {
          this.number += 1;
        }
        this.startMove();
      }, 2000)
    },
  }
}
