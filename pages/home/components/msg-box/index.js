export default {
  template: `
    <div class="msg-box" @touchmove.prevent="">
      <transition
        name="custom-classes-transition"
        enter-active-class="animated zoomIn faster"
        leave-active-class="animated zoomOut faster"
      >
        <div v-if="isShow" :class="{'wrapper-normal': type === 'DRAW_TIPS', 'wrapper-paper': type === 'DRAW_AWARD', 'wrapper-short': type !== 'DRAW_TIPS' && type !== 'DRAW_AWARD'}" :style="'backgroundImage: url(' + wrapperBg +')'">
          <div v-if="type==='DRAW_TIPS' || type==='DRAW_ENTRY_DISABLE' || type==='PHONE_INPUT'" class="btn-close" :style="'backgroundImage: url(' + msgboxBtnClose +')'" @click="close"></div>
          <div v-if="type==='DRAW_TIPS' || type==='GAME_LOSE' || type==='GAME_WIN' || type==='GMAE_RECORD'" class="title" :style="'backgroundImage: url(' + msgboxBgRedTie +')'">{{title}}</div>
          <div v-if="type === 'DRAW_AWARD'" class="draw-award-title">{{title}}</div>
          <div v-if="type==='DRAW_TIPS'" class="draw-tips-container">
            <p class="draw-tips-container-text">今天玩消消乐，还可以参与抽奖哦！</p>
            <p class="draw-tips-container-gift-text">奖品是{{bigPrizeName}}</p>
            <img class="draw-tips-container-gift" :src="bigPrizeImgUrl" alt="bigPrizeImgUrl" />
          </div>
          <div v-else-if="type==='DRAW_ENTRY_DISABLE'" class="draw-entry-disable-container">
            <p class="draw-entry-disable-container-text">抽奖入口尚未开放</p>
            <p class="draw-entry-disable-container-tiny-text">请周二10:00再来噢!</p>
          </div>
          <div v-else-if="type==='PHONE_INPUT'" class="phone-input-container">
            <p class="phone-input-container-text">填写领奖信息</p>
            <input class="phone-input-container-input" :style="'backgroundImage: url(' + msgboxBgInput +')'" type="tel" placeholder="请输入您的手机号码" v-model="phone"/>
            <p v-if="isShowPhoneWrongTips" class="phone-input-container-tips">请输入正确的手机号</p>
          </div>
          <div v-else-if="type==='GAME_LOSE'" class="game-lose-container">
            <img :src="msgboxImgCryStar" alt="msgboxImgCryStar" />
          </div>
          <div v-else-if="type==='GAME_WIN'" class="game-win-container">
            <img :src="msgboxImgTriStar" alt="msgboxImgTriStar" />
          </div>
          <div v-else-if="type==='GMAE_RECORD'" class="game-record-container">
            <p class="game-record-container-h1">您一共使用了:</p>
            <div class="game-record-container-time">
              {{curTime}}s
              <img v-if="isShowNewLabel" class="new-label" :src="msgboxImgNew" alt="msgboxImgNew" />
            </div>
            <p v-if="bestTime!==0" class="game-record-container-h2">历史最好成绩：{{bestTime}}s</p>
            <p v-if="isShowNewLabel" class="game-record-container-h3">你成功的打破了自己的最高记录哦！</p>
          </div>
          <div v-else-if="type==='DRAW_AWARD'" class="draw-award-container">
            <p class="draw-award-container-text">获得了</p>
            <p class="draw-award-container-gift-text">{{getPrizeName}}</p>
            <img class="draw-tips-container-gift" :src="getPrizeImgUrl" alt="getPrizeImgUrl" />
          </div>
          <div v-else-if="type==='DRAW_TEXT' || type==='MSG_CONFIRM'" class="draw-text-container">
            <p class="draw-text-container-text">{{text}}</p>
          </div>
          <div v-if="type==='MSG_CONFIRM'">
            <img class="msg-confirm-btn-cancel" :src="msgboxBtnCancel" alt="msgboxBtnCancel" @click="$emit('msgBox2Cancel')"/>
            <img class="msg-confirm-btn-confirm" :src="msgboxBtnConfirm" alt="msgboxBtnConfirm" @click="$emit('msgBox2Confirm')"/>
          </div>
          <div v-else class="btn-confirm" :style="'backgroundImage: url(' + msgboxBtn +')'" @click="confirm">{{btnText}}</div>
        </div>
      </transition>
    </div>
  `,
  name: 'MsgBox',
  props: {
    type: {
      type: String,
      required: true,
      validator (value) {
        return ['DRAW_TIPS', 'DRAW_ENTRY_DISABLE', 'PHONE_INPUT', 'GAME_LOSE', 'GAME_WIN', 'GMAE_RECORD', 'DRAW_AWARD', 'DRAW_TEXT', 'MSG_CONFIRM'].indexOf(value) !== -1
      }
    },
    bestTime: {
      type: Number
    },
    curTime: {
      type: Number
    },
    bigPrizeName: {
      type: String
    },
    bigPrizeImgUrl: {
      type: String
    },
    getPrizeName: {
      type: String
    },
    getPrizeImgUrl: {
      type: String
    },
    text: {
      type: String
    },
    isShowNewLabel: {
      type: Boolean
    }
  },
  data () {
    return {
      //图片
      msgboxBtn: GLOBAL_CONFIG.commonImg.msgboxBtn,
      msgboxBgRedTie: GLOBAL_CONFIG.commonImg.msgboxBgRedTie,
      msgboxBgInput: GLOBAL_CONFIG.commonImg.msgboxBgInput,
      msgboxImgCryStar: GLOBAL_CONFIG.commonImg.msgboxImgCryStar,
      msgboxImgTriStar: GLOBAL_CONFIG.commonImg.msgboxImgTriStar,
      msgboxBtnClose: GLOBAL_CONFIG.commonImg.msgboxBtnClose,
      msgboxImgNew: GLOBAL_CONFIG.commonImg.msgboxImgNew,
      msgboxBtnCancel: GLOBAL_CONFIG.commonImg.msgboxBtnCancel,
      msgboxBtnConfirm: GLOBAL_CONFIG.commonImg.msgboxBtnConfirm,

      wrapperBg: null,

      title: null,
      btnText: null,
      phone: null,

      isShow: false,
      isShowPhoneWrongTips: false
    }
  },
  watch: {
    phone () {
      this.isShowPhoneWrongTips = false
    }
  },
  created () {
    //根据不同的type选择对应的弹框背景
    switch (this.type) {
      case 'DRAW_TIPS': {
        this.wrapperBg = GLOBAL_CONFIG.commonImg.msgboxBgNormal
        this.title = '幸运星期二'
        this.btnText = '立即抽奖'
      } break
      case 'DRAW_ENTRY_DISABLE': {
        this.wrapperBg = GLOBAL_CONFIG.commonImg.msgboxBgShort
        this.btnText = '确定'
      } break
      case 'PHONE_INPUT': {
        this.wrapperBg = GLOBAL_CONFIG.commonImg.msgboxBgShort
        this.btnText = '确定'
      } break
      case 'GAME_LOSE': {
        this.wrapperBg = GLOBAL_CONFIG.commonImg.msgboxBgShort
        this.btnText = '重新来过'
        this.title = '继续努力'
      } break
      case 'GAME_WIN': {
        this.wrapperBg = GLOBAL_CONFIG.commonImg.msgboxBgShort
        this.btnText = '下一关'
        this.title = '恭喜通关'
      } break
      case 'GMAE_RECORD': {
        this.wrapperBg = GLOBAL_CONFIG.commonImg.msgboxBgShort
        this.btnText = '排行榜'
        this.title = '挑战成功'
      } break
      case 'DRAW_AWARD': {
        this.wrapperBg = GLOBAL_CONFIG.commonImg.msgboxBgPaper
        this.btnText = '确定'
        this.title = '恭喜您'
      } break
      case 'DRAW_TEXT': {
        this.wrapperBg = GLOBAL_CONFIG.commonImg.msgboxBgShort
        this.btnText = '确定'
      } break
      case 'MSG_CONFIRM': {
        this.wrapperBg = GLOBAL_CONFIG.commonImg.msgboxBgShort
      } break
      default: ;
    }
  },
  mounted () {
    this.isShow = true
  },
  methods: {
    close () {
      this.isShow = false
      setTimeout(() => {
        this.$emit('msgBoxClose')
      }, 200)
    },
    confirm () {
      if (this.type === 'PHONE_INPUT') {
        if (this.$_tools.checkMobile(this.phone)) {
          this.$emit('msgBoxConfirm', {
            type: this.type,
            phone: this.phone
          })
          this.close()
        } else {
          this.isShowPhoneWrongTips = true
        }
      } else {
        this.$emit('msgBoxConfirm', {
          type: this.type
        })
        this.close()
      }
    }
  }
}

